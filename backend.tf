terraform {
    backend "s3" {
      bucket = "proevolua-aula-terraform1"
      key = "terraform.tfstate"
      region = "us-east-1"
    }   
}